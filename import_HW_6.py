# Напишіть декоратор, який вимірює і
# виводить на екран час виконання функції
# в секундах і задекоруйте ним основну
# функцію гри з попередньої дз. Після закінчення
# гри декоратор має сповістити, скільки тривала гра.

from time import time
def timer_func(func):
    """This function shows the execution
    time of the function object passed"""
    def wrap_func(*args, **kwargs):
        t1 = time()
        result = func(*args, **kwargs)
        t2 = time()
        print(f'Функція {func.__name__!r} тривала {(t2-t1):.4f}s')
        return result
    return wrap_func

def get_age_format(age):
    age_int = int(age) % 100
    if age_int in range(10, 20) or age_int % 10 in (0, 5, 6, 7, 8, 9):
        return f'{age} років'
    elif age_int % 10 == 1:
        return f'{age} рік'
    else:
        return f'{age} роки'

def pretty_age_check(age):
    pretty_list = [11, 22, 33, 44, 55, 66, 77, 88, 99]
    if int(age) in pretty_list:
        print('О, вам {}! Який цікавий вік!'.format(age))

def get_message(age):
    if int(age) <= 7:
        return 'Тобі ж {}! Де твої батьки?'
    elif int(age) <= 16:
        return "Тобі лише {}, а це є фільм для дорослих!"
    elif int(age) >= 65:
        return "Вам {}? Покажіть пенсійне посвідчення!"
    else:
        return 'Незважаючи на те, що вам {}, білетів всеодно нема!'

def input_handler():
    age = input('Enter your age : ')

    if age.isdigit() != True:
        print('Введіть будьласка цифру < 120')
    else:
        pretty_age_check(age)
        msg = get_message(age)
        age_format = get_age_format(age)
        print(msg.format(age_format))

@timer_func
def user_choise():
    while True:
        a = input("Enter y/n to continue")
        if a == 'y':
            input_handler()
        elif a == "n":
            break
        else:
            print("Enter either y/n")

user_choise()
